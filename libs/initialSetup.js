const User = require("../models/user");
const bcrypt = require("bcryptjs");

//Inicializa el primer usuario ADMIN la primera vez que se ejecuta la API
const createAdmin = async () => {
  try {
    const count = await User.estimatedDocumentCount();
    if (count > 0) return;

    const hashedPassword = bcrypt.hashSync(process.env.ADMIN_PASS, 10);
    await Promise.all([
      new User({
        nombre: "Admin",
        apellido: "Master",
        password: hashedPassword,
        email: process.env.ADMIN_EMAIL,
        rol: "ADMIN",
      }).save(),
    ]);
  } catch (error) {
    console.error(error);
  }
};

module.exports = createAdmin;
