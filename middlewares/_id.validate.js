const joi = require("joi");

const validateIdParam = async (req, res, next) => {
  const schemaId = joi
    .object({
      _id: joi.string().required(),
    })
    .required();
  try {
    await schemaId.validateAsync(req.params);
    next();
  } catch (err) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: err.details[0].message,
      success: false,
      data: null,
    });
  }
};

module.exports = validateIdParam;
