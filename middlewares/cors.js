const cors = require("cors");

const corsOptions = {
  origin: ["*"],
  optionsSuccessStatus: 200,
  credentials: true,
};

module.exports = cors(corsOptions);
