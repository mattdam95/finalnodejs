const { Router } = require("express");
const router = Router();
const {
  getAllClothing,
  postClothing,
  putClothing,
  getClothing,
  delClothing,
} = require("../controllers/clothing.controller");

const {
  validatePutClothing,
  validatePostClothing,
} = require("../middlewares/clothing.validate");
const validateIdParam = require("../middlewares/_id.validate");
const { validateRoles } = require("../middlewares/auth.validate");

router.post(
  "",
  [validateRoles("ADMIN", "EMPLOYEE")],
  validatePostClothing,
  postClothing
);
router.get("/", getAllClothing);
router.get("/:_id", validateIdParam, getClothing);
router.put(
  "/:_id",
  [validateRoles("ADMIN", "EMPLOYEE"), validateIdParam],
  validatePutClothing,
  putClothing
);
router.delete(
  "/:_id",
  [validateRoles("ADMIN", "EMPLOYEE"), validateIdParam],
  delClothing
);

module.exports = router;
